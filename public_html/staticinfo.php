
<div class="container onlyOne">
	<div class = "panel header" style="min-width:240px; width:auto; height:auto">
		<h1 align="center"> О себе </h1>
	</div>

	<div class = "panel big" onclick="resize(this,'100%');" style="width:auto">
		<div class="imageContainer">
			<img class = "prevImage" src = "/images/static/avatar.png" />
		</div>
		<div class = "about">
			<div class="line">
				<div class="label text">ФИО: </div>
				<div class="text">Иванов Иван</div>
			</div>
			<div class="line">
				<div class="label text">Языки: </div>
				<div class="text">
				С/С++,C#,Java,Lua,JavaScript
				</div>				
			</div>
			<div class="line">
				<div class="label text">Навыки и умения: </div>
				<div class="text multiline">
				</div>
			</div>
			<div class="line">
				<div class="label text">Email: </div>
				<a href="mailto:@gmail.com" class="text">mail@gmail.com</a>
			</div>
			
		</div>
	</div>
</div>