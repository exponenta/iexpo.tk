<?php
include_once ("config.php");

function getRandomFileName($path,$sname, $extension='')
{
	$extension = $extension ? '.' . $extension : '';
	$path = $path ? $path . '/' : '';

	do {
		$name =$sname . uniqid();
		$file = $path . $name . $extension;
	} while (file_exists($file));

	return $file;
}

$db_mysqli = new mysqli($dblocation, $dbuser , $dbpasswd, $dbname);

if($db_mysqli->connect_errno){
	
    $data = array('error' => $db_mysqli->connect_error);

    echo json_encode($data);
}

$data = array();
if( isset( $_GET['upimage'] ) ){    
	
	$error = true;
	$reason = "";
	
    $ret = array();
	
    $uploaddir = './images';
	$query = "INSERT INTO images(url) VALUES(?)";
	$db_insert = $db_mysqli->prepare($query);
	$insert_name = '';
	
	if($db_insert){
		if($db_insert->bind_param("s", $insert_name)) {
			
			$error = false;
			foreach( $_FILES as $file ){
				$file_parts = pathinfo($file['name']);
				$fname = getRandomFileName($uploaddir, "img_", $file_parts['extension']);
				$insert_name = $fname;
				
				if( move_uploaded_file( $file['tmp_name'], $fname) and 	$db_insert->execute()){			
					$ret[] = array('url'=>$fname, 'id'=>$db_insert->insert_id);
				}
				else{
					$reason = "Ошибка создания файла\n" . $db_insert->error;
					$error = true;
				}
			}
		} else {
			$reason = $db_insert->error;
		}
	
		$db_insert -> close();
	} else {
		$reason = $db_mysqli->error;
	}
	
    $data = $error ? array('error' => $reason) : $ret ;
 
    echo json_encode($data);
}
