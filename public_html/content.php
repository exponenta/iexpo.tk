<?php function render_content($query_result, $db_mysqli = null) {
	while ($obj = $query_result->fetch_object()): //first while ?>						
		<div class = "panel" onclick="resize(this,'100%');" style="">
			<div class="imageContainer">
				<?php 
					$main = "images/static/noimage.png";
					$alt = "No image!";
					if($db_mysqli != null){
						$query = "SELECT * FROM images WHERE id={$obj->imagesid} LIMIT 1";
						$result = $db_mysqli->query($query);
						if($result != null){ 
							$img = $result->fetch_object();
							
							$main = $img->url;
							$alt = $img->description;
						}
					}
				?>
				<img class = "prevImage" src = '<?php echo $main ?>' alt='<?php echo $alt ?>' />
			</div>
			
			<div class = "about">
				<div class="line">
					<div class="label text">Название: </div>
					<div class="text"> <?php echo $obj->name ?> </div>
				</div>
				<div class="line">
					<div class="label text">Язык/Платформа: </div>
					<div class="text"> <?php echo $obj->langs ?> </div>
				</div>
					<?php if($obj->links != null):
					$links = explode(';', $obj->links);
					?>
					<div class="line">
						<div class="label text">
							<?php echo count($links)==1?"Ссылка: ":"Ссылки: "; ?>
						</div>
						<span class="text">
						<?php foreach($links as $lnk): ?>
							<a href="<?php echo $lnk ?>" target="_blank"> <?php echo $lnk ?> </a>
						<?php endforeach; ?>
						</span>				
					</div>
				<?php endif; ?>
				
				<?php if($obj -> autodescription == 0 and $obj->description != null): ?>
					<div class="line">
						<div class="label text">Описание:</div>
						<div class="text multiline">
							<?php echo $obj->description; ?>
						</div>
					</div>
				<?php endif;?>
			</div>
		</div>
	<?php endwhile; //first while
		  $query_result->close();		 
} //endfunction 
?>