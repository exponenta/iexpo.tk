<?php

    class DFileHelper
    {
        public static function getRandomFileName($path,$sname, $extension='')
        {
            $extension = $extension ? '.' . $extension : '';
            $path = $path ? $path . '/' : '';
     
            do {
                $name =$sname . uniqid();
                $file = $path . $name . $extension;
            } while (file_exists($file));
     
            return $file;
        }
    }

    $mapsFolder =  getcwd() . '/Maps//';
    $command = $_POST["command"];

    if($command == "send"){ // -------sendmap

       if($_FILES["file"]["size"] > 1024 * 1024)
       {
         echo "ERROR FILESIZE";
         exit;
       }

       if(is_uploaded_file($_FILES["file"]["tmp_name"]))
       {
         $fcontent = file_get_contents($_FILES["file"]["tmp_name"]);
         if($fcontent && $jsonContent = json_decode($fcontent,true)){
             if($jsonContent){
                if(!$content = file_get_contents($mapsFolder . "manifest.json") ){
                    //echo "<br>Could't open" . $mapsFolder . "manifest.json";
                    echo "ERROR MANIFESTLOST";
                    exit();
                }
                //open manifest
                if($manifestContent = json_decode($content,true)){
                    
                    foreach ($manifestContent["levels"] as $value) {
                        if($jsonContent["Name"] == $value["Name"] && $jsonContent["author"] == $value["author"]){

                            if($jsonContent["version"] == $value["version"]){
                                //echo "ERROR:Current file allready exists!!<br>";
                                echo "ERORR FILECLONE";
                                exit();
                            } else {
                                $value["version"] = $jsonContent["version"];
                                file_put_contents($mapsFolder . $value["fileName"] .".json", $fcontent);
                                //echo "Succesful:" . $mapsFolder . $value["fileName"]  . "  update <br>";
                                echo "SUCCESS FILEUPDATE";
                                exit();
                            }
                        }
                    }

                        $name = DFileHelper::getRandomFileName("",'map_');                    
                        file_put_contents($mapsFolder . $name . ".json", $fcontent);

                        $jsonContent["fileName"] = $name;
                        $jsonContent["objects"] = array();

                        array_push($manifestContent["levels"],$jsonContent);
                        $manifestContent["count"] ++;

                        file_put_contents($mapsFolder . "/manifest.json", json_encode($manifestContent,JSON_PRETTY_PRINT));
                        //echo $name . "<br>Upload succesfull<br>";
                        echo "SUCCESS FILECREATE";
                    }

                }
            }
       }
    } //--- sendmap

    if($command == "delete") {
        $name = $_POST["name"];
        if(empty($name)){
            echo "ERROR EMPTYNAME";
            exit();
        }

        if(!$content = file_get_contents($mapsFolder . "manifest.json") ){
            //echo "<br>Could't open" . $mapsFolder . "manifest.json";
            echo "ERROR MANIFESTLOST";
            exit();
        }
        //open manifest
        if($manifestContent = json_decode($content,true)){
            foreach ($manifestContent["levels"] as $key => $value) {
                if($value["fileName"] == $name){
                    unset($manifestContent["levels"][$key]);
                    sort($manifestContent["levels"]);
                    $manifestContent["count"] = count($manifestContent["levels"]);
                    file_put_contents($mapsFolder . "/manifest.json", json_encode($manifestContent,JSON_PRETTY_PRINT));

                    if(file_exists($mapsFolder . $name . ".json"))
                        unlink($mapsFolder . $name . ".json");
                    
                    echo "SUCCESS DELETE";
                    exit();
                }

            }
            echo "ERROR FILELOST";
            exit();
        }
    }

    if($command == "getmap") {
        $name = $_POST["name"];
        if(empty($name)){
            echo "ERROR EMPTYNAME";
            exit();
        }

        if(!file_exists($mapsFolder . $name . ".json")){
            echo "ERROR FILELOST";
            exit();
        }

        echo file_get_contents($mapsFolder . $name . ".json");
        exit();
    }

    if($command == "getmanifest") {

        if(!file_exists($mapsFolder . "manifest.json")){
            echo "ERROR MANIFESTLOST";
            exit();
        }

        echo file_get_contents($mapsFolder . "manifest.json");
        exit();
    }


    // если нигде не остановилось 
    // $command == inlegal

    echo "ERROR INLEGALCOMMAND";

?>

