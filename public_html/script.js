function resize (element, param) {

	var def = element.style.height;
	if(def == element.defHeight || element.defHeight == null) {
		element.style.height = param;
	} else {
		element.style.height = element.defHeight;
	}
	element.defHeight = def;

}
