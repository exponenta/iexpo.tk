<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="Description" content="Сайт-визитка Тимошенко (eXponenta, regIon) Константина, разработчика Unity3D, С#, Java">
		<link rel="image_src" href="images/static/Logo.png" />
	    <meta property = "og:title" content = "eXponenta" />
	    <meta property = "og:type" content = "article" />
		<meta property = "og:image" content = "images/static/Logo.png" />
		<meta property = "og:description" content = "Сайт-визитка Тимошенко (eXponenta, regIon) Константина, разработчика Unity3D, С#, Java" />
		<title>Резюме</title>
		<link rel="stylesheet" type="text/css" href="style.css">
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
			integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="
			crossorigin="anonymous"> </script>	
		<!--<script type="text/javascript" src='script.js'> </script> -->
	</head>
	
	<?php
	include_once("config.php");
	include_once("content.php");
	$db_mysqli = new mysqli($dblocation, $dbuser , $dbpasswd, $dbname);
?> 

	<body style = "height:100%;">
		<!-- Google Analystic -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-66111832-1', 'auto');
		  ga('send', 'pageview');
		</script>
		
		<!----WRAPER-->
		<div class="content">
		<?php if(!$db_mysqli->connect_error): ?>
		<!-- статика -->
		<?php include_once("staticinfo.php"); ?>
		<?php 
			$query = "SELECT * FROM projects WHERE status='current'";
			$is_current_there = false;
			if($result = $db_mysqli->query($query) and $result->num_rows > 0):
				$is_current_there = true;
			?>		
				<div class="container onlyOne">
					<div class = "panel header" style="min-width:240px; width:auto; height:auto">
						<h1 align="center"> <?php echo $result->num_rows == 1?"Текущий проект":"Текущие проекты" ?></h1>
					</div>
					<!-- потом пофиксить
					<div class="container autoResize" id = "current_ projects" style="padding:0;  box-shadow:none; background-color:transparent">
					-->
						<?php render_content($result, $db_mysqli); ?>
					<!--</div>-->
				</div>
			<?php endif;
			
			$query = "SELECT * FROM projects WHERE NOT status='current'";
			if($result = $db_mysqli->query($query)): ?>		
				<div class="container onlyOne">
					<div class = "panel header" style="min-width:240px; width:auto; height:auto">
						<h1 align="center"> 
							<?php
								if($is_current_there){
									echo $result->num_rows == 1?"Другой проект":"Другие проекты";
								} else {
									echo $result->num_rows == 1?"Проект":"Проекты";
								}
							?>
						</h1>
					</div>
					<div class="container autoResize" id = "projects" style="padding:0;  box-shadow:none; background-color:transparent">
						<?php render_content($result, $db_mysqli); ?>
					</div>
				</div>
			<?php endif;?>
			
		<!-- non connect to BD -->
		<?php else: ?>
			<div class = "panel header" style="min-width:240px; width:auto; height:auto">
				<h1 align="center"> Извините, база данных недоступна!</h1>
				<img class = "prevImage" src = "images/static/noimage.png" />
			</div>
		<?php
			endif;
			$db_mysqli->close() 
		?>
		<!----WRAPER END-->
		</div>
	</body>
</HTML>