var image_data = {}
var send_image = function(){
	console.log("send");
	var files = $('#file-selector').prop('files');
	
	if(files == null || files.length == 0) {
		alert("Нужно выбрать хоть один файл!");
		return;
	}
	
    var data = new FormData();
    $.each( files, function( key, value ){
        data.append( key, value );
    });
 
    // Отправляем запрос
 
    $.ajax({
        url: '../submit.php?upimage',
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false, 
        contentType: false,
    }).done(function( data, text ) {
		
		console.log(data);
		image_data = data;
		if(text === 'success' && !data.error) {
			
			$.each(image_data, function(key, value){
				$("#pre-image-container").append(`<img src = '.${value.url}' id = img_${value.id} class = "pre-image"/>`)
			});
			
		} else {
			alert("Ошибка!");
		}
		
	}).fail(function( data, text, err) {
		alert("Ошибка: " + err);	
	});
		
 
}