<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Резюме - админская панель</title>
		<link rel="stylesheet" type="text/css" href="../style.css">
		<script   src="https://code.jquery.com/jquery-3.1.1.min.js"
			integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			crossorigin="anonymous">
		 </script>
		<script type="text/javascript" src='./admin.js'> </script>
	</head>
	<body style = 'height:100%;'>
	
	<?php
		
		session_start();
		$auth = false;
		
		if(isset($_GET["logout"])){
			session_unset();
			session_destroy();
		} else {
			
			$_tmp_pass = "*********" ;// нельзя так делать!!
			
			if(isset($_POST["password"])){
				$auth = ($_POST["password"] == $_tmp_pass);
				$_SESSION["is_auth"] = $auth;
			} else {	
				if(isset($_SESSION["is_auth"])) {
					$auth = $_SESSION["is_auth"];
				}
			}
		}
		if(!$auth):
	?>
		<div class="container" style = "text-align:inherit; margin-top:200px;">
			<div class = "panel" style='display:block; width:300px; margin:auto; text-align:center;'>
				<span> Пароль: </span>
				<form method = "post" action = "/admin/">
					<input type="password"  id="pass" name = "password" style="max-height:14pt; width:100%;" value="" />
					<button type="submit" > Авторизация </button>
				</form>
			</div>
		</div>

	<?php 
		exit();
		endif;
	?>
	
		<div class="container" style = "text-align:inherit;">
			<div class = "panel header" style='display:inline-block;'>
				<a href="?logout=1" >
					Выход
				</a>
			</div>
			<div class = "panel header" style='display:inline-block;'>Редактор контента:</div>
			<br/>
			<div class = "panel" style="display:block; max-width:600px; width:auto;">
				
				<div>
					<span> Название: </span>
					<input type="textbox" id="name" class="admin-box one-line" style="max-height:14pt;" ></input>
				</div>
				</br>
				<div>
					<span> Описание: </span> 
					<br/>
					<textarea value="Описание" id="description" class="admin-box" style="min-height:80px;" ></textarea>
				</div>
				</br>
				<div>
					<span> Язык(языки)/платформа(-ы): </span>
					<input type="textbox" id="langs" class="admin-box one-line" style="max-height:14pt;"></input>
				</div>
				</br>
				<div>
					<span> Ссылки: </span>
					<input type="textbox" id="links" class="admin-box one-line" style="max-height:14pt;" ></input>
				</div>
				</br>
				<div>
					<span> Изображения: </span>
					<div id = "send-image" class = "admin-box" style="color:inherit; background:inherit;">
						<input type = "file" id="file-selector"  multiple = "true" accept="image/jpeg,image/png,image/gif">
						</br>
						</br>
						<button id = "send-image-button" onclick = "send_image();" > Загрузить изображения </button>
					</div>
					<br/>
					<div class="imageContainer" id = "pre-image-container">
					</div>
				</div>	
				</br>
				<div style = "text-align:center">
					<div style = "font-size:18pt;">
						<button id = "send-image-button" onclick = "send_editor_data();" style = "width:200px; font-size:inherit;" >Отправить</button>
						<button id = "send-image-button" onclick = "delete_data();"  style = "width:200px; font-size:inherit;" > Удалить</button>
					</div>
				</div>
				
			</div>
		</div>
	</body>
</HTML>